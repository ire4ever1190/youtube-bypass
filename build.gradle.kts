group = "youtubebypass"
version = "1.0-SNAPSHOT"

val kotlin_version = "1.3.40"
plugins {
    kotlin("jvm") version "1.3.40"
    id("com.github.johnrengelman.shadow") version "5.1.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("com.sparkjava:spark-kotlin:1.0.0-alpha")
    implementation("org.slf4j:slf4j-simple:1.7.21")
}


val jar by tasks.getting(Jar::class) {
    manifest {
        attributes["Main-Class"] = "me.jakeleahy.youtubebypass.MainKt"
    }
}