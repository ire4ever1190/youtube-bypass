FROM alpine:latest
RUN apk add --update curl python3 openjdk8-jre-base
RUN curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
RUN chmod a+rx /usr/local/bin/youtube-dl
RUN youtube-dl --version
RUN apk remove curl
RUN rm -rf /var/cache/apk/*
COPY build/libs/webapp-1.0-SNAPSHOT-all.jar app.jar
CMD ["java", "-jar", "app.jar"]