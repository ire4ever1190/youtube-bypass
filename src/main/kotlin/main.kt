package me.jakeleahy.youtubebypass
import spark.kotlin.*
import me.jakeleahy.youtubebypass.Command
val ytdlCommand = findCommand("youtube-dl")!!
fun main() {
    staticFiles.location("/public")
    staticFiles.expireTime(60*60*24*2)
    port(4568)
    get("/") {
        "/index.html".asResource()
    }
    get("/videourl") {
        val videoUrl: String = this.request.queryParams("videourl")
        videoUrl
                .replace("&&", "")
                .replace("|","")
                .replace(";", "")
        val proc = Command(ytdlCommand, argument = "-J", parameter = videoUrl).run()
        this.response.type("application/json")
        proc!!.getOutput()!!

    }

}

fun String.asResource(): String = object {}.javaClass.getResource(this).readText()