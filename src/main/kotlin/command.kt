package me.jakeleahy.youtubebypass

import java.io.File
import java.io.IOException
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

class Command(
    val command: String,
    var argument: String? = null,
    var parameter: String? = null){

    fun run (): Process?{
        try {
            val builtCommand = "$command ${if (argument == null){""} else {argument}} ${if (parameter == null){""} else {parameter}}".replace("&&", "").replace("|", "")
            println(builtCommand)
            val proc = Runtime.getRuntime().exec(builtCommand)
            proc.waitFor(60, TimeUnit.MINUTES)
            return proc

        } catch(e: IOException) {
            e.printStackTrace()
            return null
        }
    }
}
fun Process.getOutput(): String?{
    return this.inputStream.bufferedReader().readText()
}


fun findCommand(command: String): String? {
    return Command("/usr/bin/which", parameter = command).run()?.getOutput()?.substringBeforeLast("\n")
}