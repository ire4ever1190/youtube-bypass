function setVideo(url) {
    function onLoad() {
        var formatRe = /(\d+)x(\d+)/;
        var json = JSON.parse(this.responseText);
        var bestVideo = null;
        json['formats'].forEach(function (it) {
            if (it["acodec"] !== "none" && it["vcodec"] !== "none") {
                var reFind = it["format"].match(formatRe);
                var pixels = reFind[1] * reFind[2];
                if (bestVideo === null) {
                    bestVideo = it
                } else if (pixels > bestVideo) {
                    bestVideo = it
                }
            }
        });
        console.log(bestVideo["format"])
        var videoPlayer = document.getElementById("videoPlayer");
        videoPlayer.setAttribute("src", bestVideo["url"]);
        videoPlayer.setAttribute("width", bestVideo["width"]);
        videoPlayer.setAttribute("height", bestVideo["height"]);
        videoPlayer.setAttribute("poster", json["thumbnail"]);
    }

    var http = new XMLHttpRequest();
    http.addEventListener("load", onLoad);
    http.open("GET", "/videourl?videourl=" + url);
    http.send();
}

function searchOrSet(text) {
    var youtubeUrlRe = /(https:\/\/(www.)?youtube.com\/watch\?v=.*)/
    document.body.style.cursor = 'wait';
    if (youtubeUrlRe.test(text) === true) {
        setVideo(text)
    } else {
        findVideo(text)
    }
    document.body.style.cursor = 'default';
}